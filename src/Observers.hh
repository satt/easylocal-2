#ifndef _OBSERVERS_HH_
#define _OBSERVERS_HH_

/** @defgroup Observers Observer classes
      Observer classes perform actions 
  */

#include <observers/GeneralizedLocalSearchObserver.hh>
#include <observers/RunnerObserver.hh>
#include <observers/BimodalRunnerObserver.hh>
#include <observers/SimpleKickerObserver.hh>
#include <observers/BimodalKickerObserver.hh>
#include <observers/ShiftingPenaltyObserver.hh>

#endif // define _OBSERVERS_HH_
